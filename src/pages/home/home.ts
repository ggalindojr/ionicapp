import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ServidorProvider } from '../../providers/servidor/servidor';
import { DetalhesPage } from '../detalhes/detalhes';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  contatos: any;

  pessoa = [];

  constructor(public navCtrl: NavController, public servidor: ServidorProvider) {

    this.getRetornar();

  }

  getRetornar(){
    this.servidor.getPegar()
      .subscribe(
        data => this.contatos = data,
        err => console.log(err)
      );
  }

  goToEdit(reg){
    this.navCtrl.push(DetalhesPage, {pessoa: reg});
    // console.log(reg);
  }

}
