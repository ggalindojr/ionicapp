import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ServidorProvider } from '../../providers/servidor/servidor';

@IonicPage()
@Component({
  selector: 'page-detalhes',
  templateUrl: 'detalhes.html',
})
export class DetalhesPage {

  contatos: any;

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public servidor: ServidorProvider,
              public toastCtrl: ToastController) {

    let pessoaParam = this.navParams.get('pessoa');

    if (pessoaParam != null) {
      this.contatos = pessoaParam;
    }

    /* this.contatos = this.navParams.get('detalhes');
    console.log(navParams); */

  }

  salvar(reg){
    if (reg.codigo) { //console.log(reg)
      this.servidor.updateData(reg)
      this.navCtrl.pop();

      this.toastCtrl.create({
        message: "Registro atualizado",
        duration: 4000
      }).present();
      
      // err => console.log(err)

      error => {
        this.toastCtrl.create({
          message: "Erro ao atualizar",
          duration: 4000
        }).present();
         console.log(error.text());
       }

    }
  }

}
