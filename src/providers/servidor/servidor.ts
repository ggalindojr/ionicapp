import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, Headers, Response, ResponseOptions } from '@angular/http';
import { map } from 'rxjs/operators';
import 'rxjs/add/operator/map';
import { Platform } from 'ionic-angular';

/*
  Generated class for the ServidorProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ServidorProvider {

  url: string = "http://localhost:8081/appIonic/";
  api: string = "/apilocal";

  constructor(public http: Http,
              private _platform: Platform) {
      if (this._platform.is("cordova")) {
        this.api = "http://localhost:8081/appIonic";
      }
  }

  getPegar(){
    return this.http.get(this.url+'dados.php').pipe(map(res => res.json()));
  }

  updateData(param){
       const url = `${this.api}/updados.php/${param.codigo}`; //console.log(url);
       const data = JSON.stringify(param); console.log(data);
       return this.http.put(url, data)
       .subscribe(
           error => console.log(error)
       );
  }
}
